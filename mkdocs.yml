site_description: Numérique inclusif responsable et durable - NIRD-Uporu

# Contenu généré depuis les paramètres de votre espace gitlab
site_name: !ENV [CI_PROJECT_TITLE, "Version locale du site"]
site_url: !ENV [CI_PAGES_URL, "http://127.0.0.1:8000/"]
site_author: !ENV [CI_PROJECT_ROOT_NAMESPACE, USERNAME]
repo_url: !ENV [CI_PROJECT_URL]  # pour avoir le lien vers le dépôt

copyright: |
    Collège UPORU, à partir d'<a href="https://forge.apps.education.fr/romain.debailleul/NIRD">un travail de Romain DEBAILLEUL</a>
    <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>.



docs_dir: docs

#nav:  Inutile avec l'utilisation des fichiers .pages plus pratiques 


theme:
    favicon: assets/images/favicon.png
    icon:
      logo: material/cast-education
    name: pyodide-mkdocs-theme
    
    # La palette de couleur est maintenant intégrée par défaut au thème. 
    # Si vous souhaitez en changer, vous pouvez redéclarer la section entière 
    # et vos réglages prendront le pas sur les valeurs par défaut.
    # Mêmes choses pour la langue et la suppression des google-fonts (non RGPD).  
    #font: false                     # RGPD ; pas de fonte Google
    #language: fr                    # français
    #palette:                        # Palettes de couleurs jour/nuit
      #- media: "(prefers-color-scheme: light)"
        #scheme: default
        #primary: indigo
        #accent: indigo
        #toggle:
            #icon: material/weather-sunny
            #name: Passer au mode nuit
      #- media: "(prefers-color-scheme: dark)"
        #scheme: slate
        #primary: blue
        #accent: blue
        #toggle:
            #icon: material/weather-night
            #name: Passer au mode jour
    features:
        #- navigation.instant  !! Obligatoire à supprimer
        #- navigation.tabs   suppression pour avoir le menu vertical
        - navigation.top
        - toc.integrate
        - header.autohide
        - content.code.annotate   # Pour les annotations de code deroulantes avec +    
        - content.code.copy  # Ajout après MAJ pour pouvoir copier du code


markdown_extensions:
    - md_in_html
    - meta
    - abbr

    - def_list                      # Les listes de définition.
    - attr_list                     # Un peu de CSS et des attributs HTML.
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - admonition                    # Blocs colorés  !!! info "ma remarque"
    - pymdownx.details              #   qui peuvent se plier/déplier.
    - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.highlight:           # Coloration syntaxique du code
        auto_title: true
        anchor_linenums: true
        line_spans: __span
        pygments_lang_class: true
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
        custom_checkbox:    false   #   avec cases d'origine
        clickable_checkbox: true    #   et cliquables.
    - pymdownx.tabbed:              # Volets glissants.  === "Mon volet"
        alternate_style: true 

    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.emoji: # Émojis  :boom:
        emoji_index: !!python/name:material.extensions.emoji.twemoji
        emoji_generator: !!python/name:material.extensions.emoji.to_svg


    - pymdownx.superfences:
        custom_fences:
          - name: mermaid
            class: mermaid
            format: !!python/name:pymdownx.superfences.fence_code_format



    - pymdownx.arithmatex:
        generic: true
    - toc:
        permalink: ⚓︎
        toc_depth: 3

extra:
    social:
        - icon: fontawesome/solid/clipboard-question
          link: https://forge.apps.education.fr/num/nird/-/issues
          name: Poser une question sur le projet NIRD-Uporu par ticket




plugins:

  - awesome-pages:
      collapse_single_pages: true

  - material/search
  - material/tags:
      tags_file: tags.md
  - pyodide_macros:
      # Vous pouvez ajouter ici tout réglage que vous auriez ajouté concernant les macros:
      on_error_fail: true     # Il est conseillé d'ajouter celui-ci si vous ne l'utilisez pas.
      build:
        python_libs:
          - turtle
        tab_to_spaces: 4

# En remplacement de mkdocs-exclude. Tous les fichiers correspondant aux patterns indiqués seront
# exclu du site final et donc également de l'indexation de la recherche.
# Nota: ne pas mettre de commentaires dans ces lignes !
exclude_docs: |
    **/*_REM.md
    **/*.py


#extra_javascript:  Supprimé pour MAJ pyodide 
  #- xtra/mathjax.js                    # MathJax
  #- https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js

extra_css:
  - xtra/stylesheets/ajustements.css                      # ajustements
