---
author: Collège Uporu
title: 🏡 Accueil
---

# Numérique Inclusif Responsable et Durable : NIRD


Le but de ce projet est d'acculturer les élèves et la communauté éducative aux logiciels libres. Nous proposons un retour d'expérience aux enseignants qui souhaitent s'engager dans la même démarche d'utilisation des logiciels libres dans leur établissement.

>À travers le projet NIRD les élèves acquièrent une culture numérique au sens large, qui associe les dimensions sociales et éthiques à la maîtrise d’habiletés techniques. 

 Il s'agit de faciliter le lancement de ce type de projet en indiquant la démarche que nous avons suivie et en offrant un retour d'expérience documenté attestant de la force et des avantages de l'utilisation des logiciels libres. En établissement scolaire et auprès des familles. 

Ressource technique, formation des enseignants, informations pratiques, cours pour les élèves. L'ensemble de la ressource se trouve sur [Maths-code.fr](https://maths-code.fr). En voici les composantes :

#### Intégrer un client GNU/Linux sur un réseau pédagogique Eole, utilisé en Polynésie française :


#### Documentation et retour d'expérience Logiciels libres au lycée

- [ ] [Retour d'expérience](https://maths-code.fr/cours/2022/06/28/logiciel-libre-au-lycee-retour-dexperience/) : écueils, informations pratiques, points clés de réussite.  
- [ ] [Description détaillée du projet](https://maths-code.fr/cours/numerique-responsable/) : les objectifs.


#### Reconditionnement de machines par les élèves pour les élèves en **100% logiciels libres**.
Le protocole ci-dessous est utilisé par les élèves du collège Uporu de l'île de Taha'a dans le cadre d’un reconditionnement de PCs (anciens PCs du parc ou PCs rétrocédés par des entreprises, l’association de parents d’élèves ou des particuliers). 
> Ces machines reconditionnées sous GNU/Linux par les élèves sont à destination des établissements partenaires du collège (Centre des jeunes adolescents -CJA- , écoles primaires...).
    

En voici les grandes lignes :
- Nettoyage physique du PC : aspiration des poussières et soufflage pneumatique (bloquer les ventilateurs pour éviter les problèmes).
- Si nécessaire,installation d’un disque dur *SSD*, de barettes mémoires.
- Création de la clé USB « vive » à partir de l’image système téléchargée. Elle va permettre d’installer le système. Nous utilisons Ventoy pour créer une clé multiboot : plusieurs images de distribution peuvent y être stockées.
- En l’indiquant au BIOS/UEFI : démarrage sur la clé USB et installation du système à partir du système « live ». 
- Éventuellement désactiver le *SecureBoot*.
- Paramétrage divers -pilotes de cartes graphiques, écran tactiles etc.- et installation des logiciels.

Voir le [Protocole complet de reconditionnement de machines sous **logiciels libres**](https://maths-code.fr/cours/recycler-un-pc-sous-gnu-linux/)

## Visuels
![Logo du projet NIRD](images/NIRD_final_vert_transp-2.png)

## Installation
[Intégrer le poste à Eole](https://num.forge.apps.education.fr/uporu/tutos_eole/integrations/) :

Et appliquer la mise à jour des sources, puis celle des paquets :
- `sudo apt update`
- `sudo apt full-upgrade -y`

Cette dernière peut prendre un certain temps.

## Support
Via les [tickets du projet NIRD-Uporu](https://forge.apps.education.fr/num/nird/-/issues)

## Contribution
Toute contribution est la bienvenue : documentation, script, remarques.

## Contributeurs

- Pour le projet NIRD-Uporu : RUPN du collège
- Sur le projet d'origine : [Pascal Beel, Romain Debailleul, Shirley Mikolajczak](https://forge.apps.education.fr/romain.debailleul/NIRD)

## Licence
Le projet NIRD-Uporu est placé sous licence creative commons CC-BY-SA.

## État du projet
La phase en établissement commencée en 2024 suit son cours, attestant par l'usage de l'efficacité des logiciels libres en EPLE.

Sont déjà prêts à être installés (et intégrés au réseau Eole si besoin et si possible) :

- 6 postes LinuxMint Xfce (+ PrimTux standalone edition) seront mis en place dans une salle à destination d'élèves à besoins particuliers.

Sont prévus pour la rentrée :

- 5 postes (ordinateurs portables) sous PrimTux 8

Des postes commencent à être reconditionnés sous LinuxMint Xfce, PrimTux et AntiX (vieux PC 32 bits d'1 Go de Ram) :

- 1 poste sous AntiX ;
- 5 postes sous LinuxMint Xfce ;

A prévoir également :

- installation du client pronote sur le poste prof LinuxMint Xfce (pour le moment, il y a des soucis de passage du proxy pour les commandes Wine)


